﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using InovarMais.Core.Shared;
using InovarMais.Core;
using System.Configuration;

namespace KSTK
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            SetConnectionString();

            Application.Run(new Form1());
        }

        private static void SetConnectionString() {
            SQL.SetConnectionString(BdItem.Ident, ConfigurationManager.ConnectionStrings["IdentificacaoCnnStr"].ConnectionString);
            SQL.SetConnectionString(BdItem.MISI, ConfigurationManager.ConnectionStrings["MISICnnStr"].ConnectionString);
            SQL.SetConnectionString(BdItem.Profissional, ConfigurationManager.ConnectionStrings["TruncaturaProfissionalCnnStr"].ConnectionString);
            SQL.SetConnectionString(BdItem.Regular, ConfigurationManager.ConnectionStrings["TruncaEnsinoBasicoCnnStr"].ConnectionString);
            SQL.SetConnectionString(BdItem.Pessoal, ConfigurationManager.ConnectionStrings["InovarPessoalCnnStr"].ConnectionString);
        }
    }
}
