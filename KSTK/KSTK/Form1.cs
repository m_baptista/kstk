﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using kstkLib;
using InovarMais.Core.Extensions;
using System.IO;

namespace KSTK
{
    public partial class Form1 : Form
    {
        private string PosNome;
        private string CSVstr;
        public Form1()
        {
            InitializeComponent();
            PosNome = "_V1_" + Utils.DaDataParaFicheiro();
            CSVstr = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            var lPKAlunos = new List<int>();
            var lPKEE = new List<int>();

            //Matriculas
            var _mRegular = new MatriculasRegular();
            GravarFicheiro("matricula", _mRegular.GetCSV());
            lPKAlunos.AddRange(_mRegular.lPkAlunos.Distinct());
            lPKEE.AddRange(_mRegular.lPKEE.Distinct());
            textBox1.Text += "Matriculas regular " + Environment.NewLine;

            var _mProfissional = new MatriculasProfissional();
            GravarFicheiro("matricula", _mProfissional.GetCSV());
            lPKAlunos.AddRange(_mProfissional.lPkAlunos.Distinct());
            lPKEE.AddRange(_mProfissional.lPKEE.Distinct());
            textBox1.Text += "Matriculas profissional " + Environment.NewLine;

            //alunos
            var _alunos = new Alunos(lPKAlunos.Distinct().ToList());
            GravarFicheiro("aluno", _alunos.GetCVS());
            textBox1.Text += "Alunos " + Environment.NewLine;

            //EE
            var encEdu = new EncarregadosEducacao(lPKEE);
            GravarFicheiro("encedu", encEdu.GetCSV());

            textBox1.Text += "Enc. Edu. " + Environment.NewLine;
            ////Prof
            var prof = new Professores();
            GravarFicheiro("professor", prof.GetCSV());
            textBox1.Text += "Professor " + Environment.NewLine;

            ////Faltas
            var faltas = new FaltasRegular();
            GravarFicheiroAVA("assiduidadealuno", faltas.GetCSV());
            textBox1.Text += "Faltas regular " + Environment.NewLine;

            for (int ano = 2014; ano <= DateTime.Now.Year; ano++)
            {
                var faltasProd = new FaltasProfissional();
                GravarFicheiroAVA("assiduidadealuno", faltasProd.GetCSV(ano));
                textBox1.Text += "Faltas profissional " + Environment.NewLine;
            }


            ////avalicao
            var aval = new ClassificacoesRegular();
            GravarFicheiroAVA("avaliacao", aval.GetCSV());
            textBox1.Text += "avaliacao regular " + Environment.NewLine;

            var avalProf = new ClassificacoesProfissional();
            GravarFicheiroAVA("avaliacao", avalProf.GetCSV());
            textBox1.Text += "avalicao profissional " + Environment.NewLine;

            ////Assiduidade Relativa
            var assiduidadeRegular = new AssiduidadeRelativaRegular();
            GravarFicheiroAVA("AssiduidadeRelativa", assiduidadeRegular.GetCSV());
            textBox1.Text += "Assiduidade regular " + Environment.NewLine;

            var assiduidadeProf = new AssiduidadeRelativaProfissional();
            GravarFicheiroAVA("AssiduidadeRelativa", assiduidadeProf.GetCSV());
            textBox1.Text += "Assiduidade profissional " + Environment.NewLine;
            MessageBox.Show("Fim");
        }

        private void GravarFicheiro(string ficheiro, string conteudo)
        {
            if (string.IsNullOrWhiteSpace(conteudo))
                return;

            var NomeFicheiro = "c:\\temp\\EDATAFILE_kstk_" + ficheiro + PosNome + ".csv";

            File.AppendAllText(NomeFicheiro, conteudo, Encoding.UTF8);
           
        }

        private void GravarFicheiroAVA(string ficheiro, List<string> conteudo)
        {
            if (conteudo == null || conteudo.Count() ==0)
                return;

            var NomeFicheiro = "c:\\temp\\EDATAFILE_kstk_" + ficheiro + PosNome + ".csv";

            File.AppendAllLines(NomeFicheiro, conteudo, Encoding.UTF8);

        }

    }
}
