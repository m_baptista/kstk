﻿using InovarMais.Core;
using InovarMais.Core.Shared;
using System;
using System.Data;
using System.Text;
using System.Collections.Generic;

namespace kstkLib
{
    public class ClassificacoesRegular : IKSTK
    {
        private string dbAluno { get; set; }
        private string dbIdent { get; set; }

        public ClassificacoesRegular()
        {
            dbAluno = SQL.GetBdName(BdItem.Regular) + ".dbo.";
            dbIdent = SQL.GetBdName(BdItem.Ident) + ".dbo.";
        }
        public List<string> GetCSV()
        {
            var sb = new List<string>();
            //Exames
            var csv = Utils.GetCSV2(BdItem.Regular,GetSQLExames());
            if (csv != null)
                sb = csv;

            
            csv = Utils.GetCSV2(BdItem.Regular, GetSQLAvalicaoFinal());
            if (csv!=null)
                sb.AddRange(csv);

            //Periodos
            for (int i = 1; i < 4; i++)
            {
                csv = Utils.GetCSV2(BdItem.Regular,GetSQLPeriodos(i));
                if (csv != null)
                    sb.AddRange(csv);
            }
            return sb;
        }

        public string GetSQLPeriodos(int periodo)
        {
            var descP = periodo.ToString() + "P";
            var Pdesc =  "P" + periodo.ToString();

            var sql = " select  distinct " +
            "   '"+ descP + "_' + cast(lN.PK_LinhaMatriculaNota as varchar) 'ERPPK_AVALIACAO', " +
            "   m.FK_Aluno 'ERPPK_ALUNO', " +
            "   t.AnoLectivo 'ANO_LECTIVO_MATRICULA', " +
            "   '" + Pdesc + "' 'COD_PERIODO', " +
            "   '" + periodo + "º Periodo' 'NOME_PERIODO', " +
            "   'CI' 'COD_TIPO_AVALIACAO', " +
            "   'Classificação Interna' 'NOME_TIPO_AVALIACAO', " +
            "   'CI' 'COD_AVALIACAO', " +
            "   'Classificação Interna' 'NOME_AVALIACAO', " +
            "   convert(date," + Pdesc + "DataFim, 102) 'DATA_AVALIACAO', " +
            "   'REG_' + cast(D.PK_Disciplina as varchar) 'COD_DISCIPLINA', " +
            "   d.NomeExtenso 'NOME_DISCIPLINA', " +
            "   ln.Nota" + descP + " 'RESULTADO_AVALIACAO', ";
            if (periodo == 3)
                sql += " cast(lm.TransitaAno as varchar(max)) 'COD_RESULTADO_AVALIACAO', " +
                    " CASE lm.TransitaAno WHEN 1 THEN 'Passou' WHEN 0 THEN 'Reprovou' END 'NOME_RESULTADO_AVALIACAO', ";
            else
                sql += "   ''  'COD_RESULTADO_AVALIACAO', " +
                    "   ''  'NOME_RESULTADO_AVALIACAO', ";

            sql +="   OP.FK_Professor 'ERPPK_PROF_AVALIACAO', " +
            "   'N' 'AVALIACAO_OFICIAL', " +
            "   ''  'OBS_AVALIACAO', " +
            "   cast(ln.Nota" + descP + "Positiva as varchar) 'NOTA_POSITIVA', " +
            "   case ln.Nota" + descP + "Positiva when 1 then '0'  when null then null when 0 then '1' end 'NOTA_NEGATIVA' " +
            " from " + dbAluno + "M_Matricula M " +
            "   inner join " + dbAluno + "M_LinhaMatricula LM on LM.FK_Matricula = M.PK_Matricula " +
            "   inner join " + dbAluno + "M_Turma T on M.FK_Turma = T.PK_Turma " +
            "   inner join " + dbAluno + "M_LinhaMatriculaNotas LN on ln.FK_LinhaMatricula = LM.PK_LinhaMatricula " +
            "   inner join " + dbAluno + "M_OfertaTurno OT on OT.PK_OfertaTurno = lm.FK_OfertaTurno " +
            "   inner join " + dbAluno + "M_OfertaCurricular OC on Oc.PK_OfertaCurricular = OT.FK_OfertaCurricular " +
            "   inner join " + dbAluno + "C_MatrizCurricular MC on MC.PK_MatrizCurricular = OC.FK_MatrizCurricular " +
            "   inner join " + dbAluno + "C_Disciplina D on D.PK_Disciplina = MC.FK_Disciplina " +
            "   inner join " + dbAluno + "M_OfertaProfessor OP on OP.FK_OfertaTurno = OT.PK_OfertaTurno " +
            "   inner join " + dbIdent + "D_AnoLectivo AL on t.AnoLectivo = AL.Ano " +
            " where t.AnoLectivo >=2014 and ln.Nota" + descP + " is not null and op.Activo = 1 and ln.Nota" + descP + " not like '%)' and ln.Nota" + descP +" <> ''";
            return sql;
        }

        public string GetSQLAvalicaoFinal()
        {


            return " Select 'CF_' + cast(lN.PK_LinhaMatriculaNota as varchar) 'ERPPK_AVALIACAO', " +
                     "             m.FK_Aluno 'ERPPK_ALUNO',  " +
                     "             t.AnoLectivo 'ANO_LECTIVO_MATRICULA',  " +
                     "             '' 'COD_PERIODO',  " +
                     "             '' 'NOME_PERIODO',  " +
                     "             'CF' 'COD_TIPO_AVALIACAO',  " +
                     "             'Classificação Final' 'NOME_TIPO_AVALIACAO',  " +
                     "             'CF' 'COD_AVALIACAO',  " +
                     "             'Classificação Final' 'NOME_AVALIACAO',  " +
                     "             '' 'DATA_AVALIACAO',  " +
                     "             'REG_' + cast(D.PK_Disciplina as varchar) 'COD_DISCIPLINA',  " +
                     "             d.NomeExtenso 'NOME_DISCIPLINA',  " +
                     "             LMNS.Nota_CFD 'RESULTADO_AVALIACAO',  " +
                     "             '' 'COD_RESULTADO_AVALIACAO',  " +
                     "             '' 'NOME_RESULTADO_AVALIACAO',  " +
                     "             OP.FK_Professor 'ERPPK_PROF_AVALIACAO',  " +
                     "             'N' 'AVALIACAO_OFICIAL',  " +
                     "             '' 'OBS_AVALIACAO', " +
                     "             '1' 'NOTA_POSITIVA',  " +
                     "             '0' 'NOTA_NEGATIVA'  " +
                     " from " + dbAluno + "M_Matricula M  " +
                     "     inner join " + dbAluno + "M_LinhaMatricula LM on LM.FK_Matricula = M.PK_Matricula " +
                     "     inner join " + dbAluno + "M_OfertaTurno OT on OT.PK_OfertaTurno = lm.FK_OfertaTurno " +
                     "     inner join " + dbAluno + "M_Turma T on M.FK_Turma = T.PK_Turma   " +
                     "     inner join " + dbAluno + "M_LinhaMatriculaNotas LN on ln.FK_LinhaMatricula = LM.PK_LinhaMatricula " +
                     "     inner join (select FK_LinhaMatriculaNotas, Nota_CFD from " + dbAluno + "M_LinhaMatriculaNotasSecundario where Nota_CFD is not null and nota_cfd <>'0' and nota_cfd <> '')  LMNS on LMNS.FK_LinhaMatriculaNotas = LN.PK_LinhaMatriculaNota " +
                     "     inner join " + dbAluno + "M_OfertaCurricular OC on Oc.PK_OfertaCurricular = OT.FK_OfertaCurricular " +
                     "     inner join " + dbAluno + "C_MatrizCurricular MC on MC.PK_MatrizCurricular = OC.FK_MatrizCurricular " +
                     "     inner join " + dbAluno + "C_Disciplina D on D.PK_Disciplina = MC.FK_Disciplina " +
                     "     inner join " + dbAluno + "M_OfertaProfessor OP on OP.FK_OfertaTurno = OT.PK_OfertaTurno " +
                     " where t.AnoLectivo >=2014 ";
        }

        public string GetSQLExames()
        {
            return " select " +
                        "     distinct 'EX1F_' + cast(re.pk_registoExames2_3Ciclo as varchar) 'ERPPK_AVALIACAO', " +
                        "   RE.FK_Aluno 'ERPPK_ALUNO', " +
                        "     RE.AnoLectivo 'ANO_LECTIVO_MATRICULA', " +
                        "     'EX1F' 'COD_PERIODO', " +
                        "     'Exame 1ª Fase' 'NOME_PERIODO', " +
                        "     'EX' 'COD_TIPO_AVALIACAO', " +
                        "     'Exame' 'NOME_TIPO_AVALIACAO', " +
                        "     'EX' 'COD_AVALIACAO', " +
                        "     'Exame' 'NOME_AVALIACAO', " +
                        "      convert(date, DataExame, 102) 'DATA_AVALIACAO', " +
                        "     'REG_' + cast(FK_Disciplina as varchar) 'COD_DISCIPLINA', " +
                        "     d.NomeExtenso 'NOME_DISCIPLINA', " +
                        "     NotaExame1Fase 'RESULTADO_AVALIACAO', " +
                        "     case when NotaExame1Fase > 2 then 1 else 0 end  'COD_RESULTADO_AVALIACAO', " +
                        "     case when NotaExame1Fase > 2 then 'Passou' else 'Reprovou' end  'NOME_RESULTADO_AVALIACAO', " +
                        "    '' 'ERPPK_PROF_AVALIACAO', " +
                        "    'S' 'AVALIACAO_OFICIAL', " +
                        "    ''  'OBS_AVALIACAO', " +
                        "    case when NotaExame1Fase > 2 then 1 else 0 end 'NOTA_POSITIVA', " +
                        "    case when NotaExame1Fase > 2 then 0 else 1 end 'NOTA_NEGATIVA'" +
                        "  from " + dbAluno + "D_RegistoExames RE" +
                        "   inner join " + dbAluno + "C_Disciplina D on PK_Disciplina = FK_Disciplina" +
                        " where AnoLectivo >=2014 and NotaExame1Fase > 0 " +
                        " Union all" +
                        "  select " +
                        "     distinct 'EX2F_' + cast(re.pk_registoExames2_3Ciclo as varchar) 'ERPPK_AVALIACAO', " +
                        "   RE.FK_Aluno 'ERPPK_ALUNO', " +
                        "     RE.AnoLectivo 'ANO_LECTIVO_MATRICULA', " +
                        "     'EX2F' 'COD_PERIODO', " +
                        "     'Exame 2ª Fase' 'NOME_PERIODO', " +
                        "     'EX' 'COD_TIPO_AVALIACAO', " +
                        "     'Exame' 'NOME_TIPO_AVALIACAO', " +
                        "     'EX' 'COD_AVALIACAO', " +
                        "     'Exame' 'NOME_AVALIACAO', " +
                        "      convert(date, DataExame, 102) 'DATA_AVALIACAO', " +
                        "     'REG_' + cast(FK_Disciplina as varchar) 'COD_DISCIPLINA', " +
                        "     d.NomeExtenso 'NOME_DISCIPLINA', " +
                        "     NotaExame2Fase 'RESULTADO_AVALIACAO', " +
                        "     case when NotaExame2Fase > 2 then 1 else 0 end  'COD_RESULTADO_AVALIACAO', " +
                        "     case when NotaExame2Fase > 2 then 'Passou' else 'Reprovou' end  'NOME_RESULTADO_AVALIACAO', " +
                        "    '' 'ERPPK_PROF_AVALIACAO', " +
                        "    'S' 'AVALIACAO_OFICIAL', " +
                        "    ''  'OBS_AVALIACAO', " +
                        "    case when NotaExame2Fase > 2 then 1 else 0 end 'NOTA_POSITIVA', " +
                        "    case when NotaExame2Fase > 2 then 0 else 1 end 'NOTA_NEGATIVA'" +
                        "  from " + dbAluno + "D_RegistoExames RE" +
                        "   inner join " + dbAluno + "C_Disciplina D on PK_Disciplina = FK_Disciplina" +
                        " where AnoLectivo >=2014 and NotaExame2Fase > 0 ";
        }

        string IKSTK.GetCSV()
        {
            throw new NotImplementedException();
        }
    }
}
