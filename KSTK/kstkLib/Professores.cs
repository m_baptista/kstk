﻿using InovarMais.Core;
using InovarMais.Core.Shared;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace kstkLib
{
    public class Professores : IKSTK
    {
        private string dbPessoal { get; set; }
        private string dbIdent { get; set; }
        private bool temPessoal { get; set; }
        public Professores()
        {
            var bd = SQL.GetBdName(BdItem.Pessoal);
            if (bd.ToLower().Contains("pessoal") || bd.ToLower().Contains("pessoal"))
                temPessoal = true;

            dbPessoal = SQL.GetBdName(BdItem.Pessoal) + ".dbo.";
            dbIdent = SQL.GetBdName(BdItem.Ident) + ".dbo.";
        }
        public string GetCSV()
        {
            DataTable dtPessoal;

            if (temPessoal)
            {
                dtPessoal = SQL.Q(BdItem.Pessoal, GetSQLDadosDocente());

                var dtHab = SQL.Q(BdItem.Pessoal, GetSQLDadosDocenteHabilitacoes());

                dtPessoal.Columns["COD_FORM_ACADEMICA_PROF"].ReadOnly = false;
                dtPessoal.Columns["COD_FORM_ACADEMICA_PROF"].MaxLength = 6;
                dtPessoal.Columns["NOME_FORM_ACADEMICA_PROF"].ReadOnly = false;
                dtPessoal.Columns["NOME_FORM_ACADEMICA_PROF"].MaxLength = 100;


                foreach (DataRow rw in dtPessoal.Rows)
                {
                    var pk = rw["PK_Pessoal"];
                    var srch = dtHab.Select("FK_Pessoal=" + pk);
                    if (srch.Length == 0)
                        continue;

                    var hab = CalculaHabilitacao(rw, srch);
                    rw["COD_FORM_ACADEMICA_PROF"] = hab[0];
                    rw["NOME_FORM_ACADEMICA_PROF"] = hab[1];

                }

            }
            else
                dtPessoal = SQL.Q(BdItem.Ident, GetSQLDadosDocenteSemPessoal());

            if (dtPessoal == null || dtPessoal.Rows.Count == 0)
                return "";
            
            return Utils.CriaCVS(dtPessoal, 1);
        }
        private ArrayList CalculaHabilitacao(DataRow linhaPessoal, DataRow[] hab)
        {
            ArrayList ret = new ArrayList();
            var lhab = hab.Select(row => row.Field<int>("Habilitacao")).ToList();

            List<int> Doutoramento = new List<int>() { 5 };
            List<int> Mestrado = new List<int>() { 4, 14, 15, 16 };
            List<int> Pos = new List<int>() { 3 };
            List<int> Licenciatura = new List<int>() { 1, 17, 18, 19 };
            List<int> Bacharelato = new List<int>() { 2, 20, 21, 22 };

            var th = TemHabilitacao(lhab, Doutoramento, "5", "Doutoramento");
            if (th != null)
                return th;

            th = TemHabilitacao(lhab, Mestrado, "4", "Mestrado");
            if (th != null)
                return th;

            th = TemHabilitacao(lhab, Pos, "3", "Pós graducação");
            if (th != null)
                return th;

            th = TemHabilitacao(lhab, Licenciatura, "1", "Licenciatura");
            if (th != null)
                return th;

            th = TemHabilitacao(lhab, Bacharelato, "3", "Bacharelato");
            if (th != null)
                return th;

            if (lhab.Contains(6))
                return new ArrayList() { "6", "Diploma de Estudos Superiores Especializados" };

            if (lhab.Contains(7))
                return new ArrayList() { "7", "Magistério Primário/Educadores de Infância" };

            if (lhab.Contains(8))
                return new ArrayList() { "8", "Secundário" };

            if (lhab.Contains(9))
                return new ArrayList() { "9", "Básico (3º ciclo)" };

            if (lhab.Contains(10))
                return new ArrayList() { "10", "Básico (2º ciclo)" };

            if (lhab.Contains(11))
                return new ArrayList() { "11", " Básico (1º ciclo)" };

            if (lhab.Contains(13))
                return new ArrayList() { "13", "Sem Habilitação" };

            if (lhab.Contains(23))
                return new ArrayList() { "23", "Outros + Formação Complementar" };

            if (lhab.Contains(24))
                return new ArrayList() { "24", "Curso de Estudos Superiores Especializados" };

            if (lhab.Contains(25))
                return new ArrayList() { "25", "Curso de Promotora em Educação de Infância" };

            return new ArrayList() { "12", "Outra" };
        }
        private ArrayList TemHabilitacao(List<int> lhab, List<int> lstCodigos, string codHab, string descrHab)
        {
            var inter = lhab.Intersect(lstCodigos);
            if (inter == null || inter.Count() == 0)
                return null;

            return new ArrayList() { codHab, descrHab }; 
        }
        private string GetSQLDadosDocente()
        {
            return string.Format(@" SELECT  
                       PK_Professor 'ERPPK_PROF',  
                       DP.Nome 'NOME_PROF',  
                       DP.NumeroContribuinte 'NIF_PROF',  
                       P.Sexo 'SEXO_PROF',  
                       CONVERT(DATE, P.DATA_NAS, 102) 'DATA_NASC_PROF',  
                       '' 'NATURALIDADE_COD_PAIS_PROF',  
                       '' 'NATURALIDADE_NOME_PAIS_PROF', 
                       P.MORADA 'MORADA_PROF',  
                       CP.Codigo + '-' + ISNULL(CP.CP3, '000')  'MORADA_CPOSTAL_PROF',  
                       CP.Localidade 'MORADA_CPOSTAL_DESC_PROF',  
                       '' 'MORADA_COD_PAIS_PROF',  
                       '' 'MORADA_NOME_PAIS_PROF', 
                       DP.EMAIL 'MAIL_PROF',  
                       DP.Telefone 'TELEFONE_PROF', 
                       '' 'COD_FORM_ACADEMICA_PROF', 
                       '' 'NOME_FORM_ACADEMICA_PROF', 
                       '' 'COD_AREA_PROFISSIONAL_PROF', 
                       '' 'NOME_AREA_PROFISSIONAL_PROF', 
                       AC.PK_MEAcessos 'COD_VINCULO_PROF', 
                       AC.VinculoContinente 'NOME_VINCULO_PROF', 
                       P.DATAINIC 'DATA_INICIO_VINCULO_PROF',  
                       ID_Categoria 'COD_STATUS_VINCULO_PROF', 
                       CPP.CATEGORIA 'NOME_STATUS_VINCULO_PROF', 
                       P.PK_Pessoal
                     FROM {0}D_Professor DP 
                       INNER JOIN {1}Pessoal P ON P.CF = NumeroContribuinte 
                       INNER JOIN {1}T_CODIGOS_POSTAIS CP ON CP.PK_CodigoPostal = P.ID_CodPostal 
                       INNER JOIN {1}ME_Acessos AC ON AC.PK_MEAcessos = Vinculo 
                       INNER JOIN {1}CategoriaProfissional CPP ON CPP.PK_CategoriaProfissional = ID_Categoria ", dbIdent, dbPessoal);
                    //" WHERE P.DOCENTE = 'S' ";
        }

        private string GetSQLDadosDocenteSemPessoal()
        {
            return string.Format(@" SELECT  
                       PK_Professor 'ERPPK_PROF',  
                       DP.Nome 'NOME_PROF',  
                       DP.NumeroContribuinte 'NIF_PROF',  
                       '' 'SEXO_PROF',  
                       '' 'DATA_NASC_PROF',  
                       '' 'NATURALIDADE_COD_PAIS_PROF',  
                       '' 'NATURALIDADE_NOME_PAIS_PROF',  
                       '' 'MORADA_PROF',  
                       ''  'MORADA_CPOSTAL_PROF',  
                       '' 'MORADA_CPOSTAL_DESC_PROF', 
                       '' 'MORADA_COD_PAIS_PROF', 
                       '' 'MORADA_NOME_PAIS_PROF',
                       DP.EMAIL 'MAIL_PROF', 
                       DP.Telefone 'TELEFONE_PROF',  
                       '' 'COD_FORM_ACADEMICA_PROF', 
                       '' 'NOME_FORM_ACADEMICA_PROF',
                       '' 'COD_AREA_PROFISSIONAL_PROF',
                       '' 'NOME_AREA_PROFISSIONAL_PROF',
                       '' 'COD_VINCULO_PROF',  
                       '' 'NOME_VINCULO_PROF',  
                       '' 'DATA_INICIO_VINCULO_PROF',
                       '' 'COD_STATUS_VINCULO_PROF',
                       '' 'NOME_STATUS_VINCULO_PROF',
                       '' 'PK_Pessoal' 
                     FROM {0}D_Professor DP ", dbIdent);
                  
        }
        private string GetSQLDadosDocenteHabilitacoes()
        {
            return string.Format( @" SELECT  FK_Pessoal, Habilitacao, fa.FormacaoAcademica  
                     FROM {0}PessoalHabilitacoes PH 
                       INNER JOIN {0}ME_FormacaoAcademica FA on FA.PK_FormacaoAcademica = ph.Habilitacao 
                     ORDER BY FK_Pessoal",dbPessoal);
        }
    }
}
