﻿using InovarMais.Core;
using InovarMais.Core.Extensions.Data;
using InovarMais.Core.Shared;
using System;
using System.Data;
using System.Text;
using System.Collections.Generic;

namespace kstkLib
{
    public class ClassificacoesProfissional : IKSTK
    {
        private string dbProf { get; set; }

        public ClassificacoesProfissional()
        {
            dbProf = SQL.GetBdName(BdItem.Profissional) + ".dbo.";
        }

        public List<string> GetCSV()
        {
            var sb = new List<string>();
            
            var csv = Utils.GetCSV2(BdItem.Profissional, GetSQL());
            if (csv!=null)
                sb.AddRange(csv);

            sb.AddRange(GetCSV_AvaliacoesPAP_FCT_NC());

            return sb;
        }

        private string GetSQL()
        {
            return string.Format(@" select   
                        distinct 'NF_' + cast(lM.PK_LinhaMatricula as varchar) 'ERPPK_AVALIACAO',  
                        M.FK_Aluno 'ERPPK_ALUNO',  
                        T.AnoLectivo 'ANO_LECTIVO_MATRICULA',  
                        '' 'COD_PERIODO',  
                        '' 'NOME_PERIODO', 
                        'NF' 'COD_TIPO_AVALIACAO',  
                        'Nota Final' 'NOME_TIPO_AVALIACAO',  
                        'NF' 'COD_AVALIACAO',  
                        'Nota Final' 'NOME_AVALIACAO',  
                        (select convert(date,max(DataNota),102) from {0}M_LinhaMatriculaModulo LMM Where LM.PK_LinhaMatricula = LMM.FK_LinhaMatricula) 'DATA_AVALIACAO',  
                        'PROF_' + cast(D.PK_Disciplina as varchar) 'COD_DISCIPLINA',  
                        d.NomeExtenso 'NOME_DISCIPLINA',  
                        lm.ClassificacaoFinal 'RESULTADO_AVALIACAO',  
                        1  'COD_RESULTADO_AVALIACAO',  
                        'Passou' 'NOME_RESULTADO_AVALIACAO',  
                        PT.FK_Professor 'ERPPK_PROF_AVALIACAO',  
                        'N' 'AVALIACAO_OFICIAL',  
                        ''  'OBS_AVALIACAO',  
                        1 'NOTA_POSITIVA',  
                        0 'NOTA_NEGATIVA' 
                     from {0}M_Matricula M  
                       inner join {0}M_LinhaMatricula LM on LM.FK_Matricula = M.PK_Matricula  
                       inner join {0}M_OfertaTurno OT on OT.PK_OfertaTurno = lm.FK_OfertaTurno  
                       inner join {0}M_OfertaCurricular OC on Oc.PK_OfertaCurricular = OT.FK_OfertaCurricular  
                       inner join {0}M_Turma T on OC.FK_Turma = T.PK_Turma  
                       inner join {0}C_DistribuicaoCurricular DC on OC.FK_DistribuicaoCurricular = PK_DistribuicaoCurricular 
                       inner join {0}C_MatrizCurricular MC on MC.PK_MatrizCurricular = DC.FK_MatrizCurricular  
                       inner join {0}C_Disciplina D on D.PK_Disciplina = MC.FK_Disciplina  
                       inner join {0}M_ProfessorTurno PT on PT.FK_OfertaTurno = OT.PK_OfertaTurno
                        inner join ( select FK_Disciplina, fk_aluno, min(PK_LinhaMatricula) pk_linhamatricula
				                        from {0}M_Matricula M  
					                        inner join {0}M_LinhaMatricula LM on LM.FK_Matricula = M.PK_Matricula  
					                        inner join {0}M_OfertaTurno OT on OT.PK_OfertaTurno = lm.FK_OfertaTurno  
					                        inner join {0}M_OfertaCurricular OC on Oc.PK_OfertaCurricular = OT.FK_OfertaCurricular  
					                        inner join {0}M_Turma T on OC.FK_Turma = T.PK_Turma  
					                        inner join {0}C_DistribuicaoCurricular DC on OC.FK_DistribuicaoCurricular = PK_DistribuicaoCurricular 
					                        inner join {0}C_MatrizCurricular MC on MC.PK_MatrizCurricular = DC.FK_MatrizCurricular  
					                        inner join {0}C_Disciplina D on D.PK_Disciplina = MC.FK_Disciplina  
					                        inner join {0}M_ProfessorTurno PT on PT.FK_OfertaTurno = OT.PK_OfertaTurno 
				                        where t.AnoLectivo >=2014 and ClassificacaoFinal > 0 and lm.ClassificacaoFinal not like '%)'
				                        group by FK_Disciplina, fk_aluno) as GRP on GRP.FK_Aluno = m.FK_Aluno and grp.FK_Disciplina = d.PK_Disciplina and grp.pk_linhamatricula = lm.PK_LinhaMatricula
                     where t.AnoLectivo >=2014 and ClassificacaoFinal > 0 and lm.ClassificacaoFinal not like '%)'", dbProf);
        }

        private List<string> GetCSV_AvaliacoesPAP_FCT_NC()
        {
            var sql = string.Format(@"select PK_Habilitacao, pap, fct, MediaCurso, DataCartaCurso, AnoLectivoConclusao, FK_Aluno 
 from {0}M_Habilitacao  
 inner join {0}M_Matricula on PK_Matricula = FK_Matricula
 where DataCartaCurso is not null AND AnoLectivoConclusao >=2014 ", dbProf);

            var dt = SQL.Q(BdItem.Profissional, sql);
            var linhasAvaliacao = new List<string>();
            if (dt.Rows.Count == 0)
                return null;
            
            foreach (DataRow dr in dt.Rows)
            {
                //Pap
                linhasAvaliacao.Add(RetornaAvaliacao("PAP", "PAP", dr));
                
                //FCT
                linhasAvaliacao.Add(RetornaAvaliacao("FCT", "FCT", dr));

                //Nota Curso
                linhasAvaliacao.Add(RetornaAvaliacao("NC", "Nota Curso", dr));
            }

            return linhasAvaliacao;
        }

        private string RetornaAvaliacao(string CodAvaliacao, string DescricaoAvaliacao ,DataRow dr)
        {
            var valores = new System.Collections.ArrayList();
            var codAva = DescricaoAvaliacao == "Nota Curso" ? "MediaCurso" : CodAvaliacao;
            
            valores.Add(((char)34) + CodAvaliacao + "_" + dr.Try("PK_Habilitacao",0).ToString() + ((char)34)); // 'ERPPK_AVALIACAO'
            valores.Add(((char)34) + dr.Try("FK_Aluno", "") + ((char)34));                                     // 'ERPPK_ALUNO'
            valores.Add(((char)34) + dr.Try("AnoLectivoConclusao") + ((char)34));                              // 'ANO_LECTIVO_MATRICULA'
            valores.Add(((char)34) + "" + ((char)34));                                                         // 'COD_PERIODO'
            valores.Add(((char)34) + "" + ((char)34));                                                         // 'NOME_PERIODO'
            valores.Add(((char)34) + "CI" + ((char)34));                                                       // 'COD_TIPO_AVALIACAO'
            valores.Add(((char)34) + "Classificação Interna" + ((char)34));                                    // 'NOME_TIPO_AVALIACAO'
            valores.Add(((char)34) + CodAvaliacao + ((char)34));                                               // 'NF' 'COD_AVALIACAO'
            valores.Add(((char)34) + DescricaoAvaliacao + ((char)34));                                         // 'Nota Final' 'NOME_AVALIACAO'
            valores.Add(((char)34) + ((DateTime)dr.Try("DataCartaCurso", new DateTime())).ToString("yyyy-MM-dd") + ((char)34));                               // 'DATA_AVALIACAO'
            valores.Add(((char)34) + "" + ((char)34));                                                         // 'COD_DISCIPLINA'
            valores.Add(((char)34) + "" + ((char)34));                                                         // 'NOME_DISCIPLINA'
            valores.Add(((char)34) + dr.Try(codAva,0) + ((char)34));                                                     // 'RESULTADO_AVALIACAO'
            valores.Add(((char)34) + "1" + ((char)34));                                                        // 'COD_RESULTADO_AVALIACAO'
            valores.Add(((char)34) + "Passou" + ((char)34));                                                   // 'NOME_RESULTADO_AVALIACAO'
            valores.Add(((char)34) + "" + ((char)34));                                                         // 'ERPPK_PROF_AVALIACAO'
            valores.Add(((char)34) + "N" + ((char)34));                                                        // 'N' 'AVALIACAO_OFICIAL'
            valores.Add(((char)34) + "" + ((char)34));                                                         // 'OBS_AVALIACAO'
            valores.Add(((char)34) + "" + ((char)34));                                                         // 'NOTA_POSITIVA'
            valores.Add(((char)34) + "" + ((char)34));                                                         // 'NOTA_NEGATIVA'
            return string.Join(",", valores.ToArray());
        }

        string IKSTK.GetCSV()
        {
            throw new NotImplementedException();
        }
    }
}
