﻿using System.Collections.Generic;
using InovarMais.Core;
using InovarMais.Core.Shared;
using System.Data;
using System;

namespace kstkLib
{
    public class MatriculasRegular : IKSTK
    {
        private string dbAlunos { get; set; }
        private string dbMisi { get; set; }
        private string dbIdent { get; set; }

        public List<int> lPkAlunos;
        public List<int> lPKEE;

        public MatriculasRegular()
        {
            dbAlunos = SQL.GetBdName(BdItem.Regular) + ".dbo.";
            dbIdent = SQL.GetBdName(BdItem.Ident) + ".dbo.";
            dbMisi = SQL.GetBdName(BdItem.MISI) + ".dbo.";
            lPkAlunos = new List<int>();
            lPKEE = new List<int>();
        }
      
        public string GetCSV()
        {
            DataTable dt;
          
                dt = SQL.Q(BdItem.Regular, GetSQL());
                if (dt.Rows.Count == 0)
                    return "";

                lPkAlunos.Clear();
                lPKEE.Clear();

                dt.Columns["COD_RESULTADO_MATRICULA"].ReadOnly = false;
                dt.Columns["NOME_RESULTADO_MATRICULA"].ReadOnly = false;

            dt.Columns["COD_RESULTADO_MATRICULA"].MaxLength = 50;
            dt.Columns["NOME_RESULTADO_MATRICULA"].MaxLength = 50;
            foreach (DataRow dr in dt.Rows)
            {
                var fkNivel = 0;
                int.TryParse(dr["COD_NIVEL_ESCOLAR_GRUPO"].ToString(), out fkNivel);

                var situacaoMatricula = Utils.CalculaSituacao(dr["NOME_RESULTADO_MATRICULA"].ToString(), fkNivel);

                dr["COD_RESULTADO_MATRICULA"] = situacaoMatricula.Codigo;
                dr["NOME_RESULTADO_MATRICULA"] = situacaoMatricula.Descricao;

                var pkAluno = 0;
                int.TryParse(dr["ERPPK_ALUNO"].ToString().Replace("REG_", ""), out pkAluno);
                lPkAlunos.Add(pkAluno);

                var pkEE = 0;
                int.TryParse(dr["ERPPK_EE"].ToString().Replace("REG_", ""), out pkEE);
                lPKEE.Add(pkEE);

            }
            return Utils.CriaCVS(dt);
        }  
        public string GetSQL()
        {
            return string.Format(@" SELECT  distinct 
                       'REG_' + cast(m.pk_matricula as varchar) 'ERPPK_MATRICULA', 
                       m.FK_Aluno 'ERPPK_ALUNO', 
                       t.AnoLectivo 'ANO_LECTIVO_MATRICULA', 
                       s.Abreviatura 'COD_STATUS_MATRICULA', 
                       s.Descricao 'NOME_STATUS_MATRICULA', 
                       ISNULL(convert(date,m.DataSituacaoDaMatricula, 102), convert(date,m.DataMatricula,102)) 'DATA_STATUS_MATRICULA', 
                       '' 'COD_RESULTADO_MATRICULA', 
                       CASE ISNULL(TransitaExtenso, '') WHEN '' THEN CASE ISNULL(AM.Avaliacao,'') when '' then 'N/A' else AM.Avaliacao END ELSE M.TransitaExtenso END 'NOME_RESULTADO_MATRICULA', 
                       '' 'DATA_RESULTADO_MATRICULA', 
                       isnull(agrpMisi.Cod_Agrupamento, escMisi.Cod_Escola) 'COD_AGRUPAMENTO', 
                       isnull(agrpMisi.Nome_Agrupamento, escMisi.Nome) 'NOME_AGRUPAMENTO', 
                       escMisi.Cod_Escola 'COD_ESCOLA', 
                       escMisi.Nome 'NOME_ESCOLA', 
                       'REG_' + cast(t.PK_Turma as varchar)'COD_TURMA', 
                       anesc.NomeAbreviadoParaTurma + ' ' + t.Designacao 'NOME_TURMA', 
                       anesc.PK_NomeAnoEscolar 'COD_NIVEL_ESCOLAR', 
                       anesc.Designacao 'NOME_NIVEL_ESCOLAR', 
                       m.fk_nivel 'COD_NIVEL_ESCOLAR_GRUPO', 
                       case anEsc.FK_NivelEnsino WHEN 5 THEN 'SEC' ELSE anEsc.NomeExtensoParaTurma END 'NOME_NIVEL_ESCOLAR_GRUPO', 
                       c.PK_Curso 'COD_CURSO', 
                       c.Nome + ' ' + c.DecretoLei 'NOME_CURSO', 
                       CASE WHEN (M.EscalaoAse IS NULL) THEN '--'  ELSE M.EscalaoAse  END AS 'COD_ASE', 
                       CASE WHEN (M.EscalaoAse IS NULL) THEN '--'  ELSE M.EscalaoAse  END AS  'NOME_ASE', 
                       '' 'DATA_INICIO_ASE', '' 'VALOR_RENDIMENTO', '' 'VALOR_HABITACAO', '' 'VALOR_DESPESAS_SAUDE', 
                       i.NElementosAgregado 'NR_PESSOAS_AGREGADO', 
                       CASE ISNULL(m.AlunoComEnsinoArticulado, 0) WHEN 0 THEN 'N' WHEN 1 THEN 'S' ELSE 'N' END 'TEM_ENSINO_ARTICULADO', 
                       CASE ISNULL(m.TemComputador, 0) WHEN 0 THEN 'N' WHEN 1 THEN 'S' ELSE 'N' END  'TEM_COMPUTADOR', 
                       CASE ISNULL(m.TemInternet, 0) WHEN 0 THEN 'N' WHEN 1 THEN 'S' ELSE 'N' END  'TEM_INTERNET', 
                       CASE ISNULL(m.UsaTransporte, 0) WHEN 0 THEN 'N' WHEN 1 THEN 'S' ELSE 'N' END  'TEM_TRANSPORTE_ESCOLAR', 
                       CASE ISNULL(m.AutorizaSairLivre, 0) WHEN 0 THEN 'N' WHEN 1 THEN 'S' ELSE 'N' END  'TEM_SAIDA_LIVRE', 
                       CASE ISNULL(m.AutorizaSairAlmoco, 0) WHEN 0 THEN 'N' WHEN 1 THEN 'S' ELSE 'N' END  'TEM_SAIDA_ALMOCO', 
                       CASE ISNULL(m.AutorizaSairTempo, 0) WHEN 0 THEN 'N' WHEN 1 THEN 'S' ELSE 'N' END  'TEM_SAIDA_ULT_TEMPO', 
                       'REG_' + cast(m.FK_EncarregadoEducacao as varchar)  'ERPPK_EE' 
                     FROM {0}M_Matricula M 
                       INNER JOIN {0}m_turma t on PK_Turma = FK_Turma 
                       INNER JOIN {1}[SituacaoMatricula] s on FK_SituacaoDaMatricula = PK_SituacaoMatricula 
                       INNER JOIN {2}D_EscolasAgrupamento escAgrp on PK_EscolaAgrupamento = T.FK_Escola 
                       INNER JOIN {1}Escolas_MISI escMisi on  escMisi.Cod_Escola = escAgrp.CodigoEscolaMISI 
                       LEFT JOIN {1}Agrupamentos_MISI agrpMisi on agrpMisi.Cod_Agrupamento = escMisi.Agrup_ID 
                       INNER JOIN {2}D_NomeAnoEscolar anEsc on anEsc.PK_NomeAnoEscolar = t.FK_Ano 
                       INNER JOIN {0}M_LinhaMatricula on PK_Matricula = FK_Matricula 
                       INNER JOIN {0}M_OfertaTurno on PK_OfertaTurno = FK_OfertaTurno  
                       INNER JOIN {0}M_OfertaCurricular on PK_OfertaCurricular = FK_OfertaCurricular  
                       INNER JOIN {0}C_MatrizCurricular on PK_MatrizCurricular = FK_MatrizCurricular 
                       INNER JOIN {0}C_Curso c on PK_Curso = FK_Curso 
                       INNER JOIN {2}I_IdentAluno I on PK_IdentAluno = m.FK_Aluno
   	                   left join {0}M_AvaliacaoMatriculaTurma AMT on AMT.FK_Matricula = m.PK_Matricula AND AMT.FK_Turma = m.FK_Turma
	                   left join {0}D_AvaliacaoMatricula AM on AM.PK_AvaliacaoMatricula = AMT.FK_AvaliacaoMatricula
 
                     WHERE t.AnoLectivo >= 2014 ", dbAlunos, dbMisi, dbIdent);
        }
    }
}
