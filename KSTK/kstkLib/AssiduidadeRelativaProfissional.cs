﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InovarMais.Core;
using InovarMais.Core.Shared;
using System.Data;

namespace kstkLib
{
    public class AssiduidadeRelativaProfissional : IKSTK
    {
        private string dbProf { get; set; }

        public AssiduidadeRelativaProfissional()
        {
            dbProf = SQL.GetBdName(BdItem.Profissional) + ".dbo.";
        }

        public List<string> GetCSV() 
        {
            DataTable dt;
            
                dt = SQL.Q(BdItem.Profissional, GetSQL());
                if (dt.Rows.Count == 0)
                    return null;
           
            return Utils.CriaCVS2(dt, -1);
        }
        public string GetSQL()
        {
            return string.Format(@" SELECT DISTINCT M.FK_Aluno 'ERPPK_ALUNO', 'PROF_' + CAST(OC.FK_Turma AS VARCHAR) 'COD_TURMA', 'PROF_' + cast(MC.FK_Disciplina as varchar) 'COD_DISCIPLINA', OT.Numero 'Turno', tm.AnoLectivo, PS.Total 'AulasDadas', '1P' 'PeriodoLetivo'
                     FROM {0}M_Matricula M  
                       INNER JOIN {0}M_LinhaMatricula LM ON M.PK_Matricula = LM.FK_Matricula 
                       INNER JOIN {0}M_OfertaTurno OT ON OT.PK_OfertaTurno = LM.FK_OfertaTurno 
                       INNER JOIN {0}M_OfertaCurricular OC ON OC.PK_OfertaCurricular = OT.FK_OfertaCurricular 
                       INNER JOIN {0}C_DistribuicaoCurricular DC ON DC.PK_DistribuicaoCurricular = OC.FK_DistribuicaoCurricular 
                       INNER JOIN {0}C_MatrizCurricular MC ON MC.PK_MatrizCurricular = DC.FK_MatrizCurricular 
                       INNER JOIN {0}M_Turma TM ON TM.PK_Turma = OC.FK_Turma
                       INNER JOIN (SELECT FK_OfertaTurno, FK_Turma, FK_Disciplina, COUNT(PK_ProfessorSumario) Total 
                             FROM {0}P_ProfessorSumario PS 
                               INNER JOIN {0}M_Turma T ON T.PK_Turma = PS.FK_Turma
                               INNER JOIN {0}D_AnoLectivo AN ON AN.Ano = T.AnoLectivo 
                             WHERE T.AnoLectivo = 2016 AND PS.DataSumario <= P1DataFim 
                             GROUP BY FK_Turma, FK_OfertaTurno,  FK_Disciplina) as PS ON PS.FK_Disciplina = MC.FK_Disciplina and ps.FK_Turma = OC.FK_Turma and ps.FK_OfertaTurno = OT.PK_OfertaTurno 

                    UNION ALL

                    SELECT DISTINCT M.FK_Aluno 'ERPPK_ALUNO', 'PROF_' + CAST(OC.FK_Turma AS VARCHAR) 'COD_TURMA', 'PROF_' + cast(MC.FK_Disciplina as varchar) 'COD_DISCIPLINA', OT.Numero 'Turno', tm.AnoLectivo, PS.Total 'AulasDadas', '2P' 'PeriodoLetivo'
                     FROM {0}M_Matricula M  
                       INNER JOIN {0}M_LinhaMatricula LM ON M.PK_Matricula = LM.FK_Matricula 
                       INNER JOIN {0}M_OfertaTurno OT ON OT.PK_OfertaTurno = LM.FK_OfertaTurno 
                       INNER JOIN {0}M_OfertaCurricular OC ON OC.PK_OfertaCurricular = OT.FK_OfertaCurricular 
                       INNER JOIN {0}C_DistribuicaoCurricular DC ON DC.PK_DistribuicaoCurricular = OC.FK_DistribuicaoCurricular 
                       INNER JOIN {0}C_MatrizCurricular MC ON MC.PK_MatrizCurricular = DC.FK_MatrizCurricular 
                       INNER JOIN {0}M_Turma TM ON TM.PK_Turma = OC.FK_Turma
                       INNER JOIN (SELECT FK_OfertaTurno, FK_Turma, FK_Disciplina, COUNT(PK_ProfessorSumario) Total 
                             FROM {0}P_ProfessorSumario PS 
                               INNER JOIN {0}M_Turma T ON T.PK_Turma = PS.FK_Turma
                               INNER JOIN {0}D_AnoLectivo AN ON AN.Ano = T.AnoLectivo 
                             WHERE T.AnoLectivo = 2016 AND ( PS.DataSumario >= P2DataInicio AND PS.DataSumario <= P2DataFim)
                             GROUP BY FK_Turma, FK_OfertaTurno,  FK_Disciplina) as PS ON PS.FK_Disciplina = MC.FK_Disciplina and ps.FK_Turma = OC.FK_Turma and ps.FK_OfertaTurno = OT.PK_OfertaTurno 

                    UNION ALL

                    SELECT DISTINCT M.FK_Aluno 'ERPPK_ALUNO', 'PROF_' + CAST(OC.FK_Turma AS VARCHAR) 'COD_TURMA', 'PROF_' + cast(MC.FK_Disciplina as varchar) 'COD_DISCIPLINA', OT.Numero 'Turno', tm.AnoLectivo, PS.Total 'AulasDadas', '3P' 'PeriodoLetivo'
                     FROM {0}M_Matricula M  
                       INNER JOIN {0}M_LinhaMatricula LM ON M.PK_Matricula = LM.FK_Matricula 
                       INNER JOIN {0}M_OfertaTurno OT ON OT.PK_OfertaTurno = LM.FK_OfertaTurno 
                       INNER JOIN {0}M_OfertaCurricular OC ON OC.PK_OfertaCurricular = OT.FK_OfertaCurricular 
                       INNER JOIN {0}C_DistribuicaoCurricular DC ON DC.PK_DistribuicaoCurricular = OC.FK_DistribuicaoCurricular 
                       INNER JOIN {0}C_MatrizCurricular MC ON MC.PK_MatrizCurricular = DC.FK_MatrizCurricular 
                       INNER JOIN {0}M_Turma TM ON TM.PK_Turma = OC.FK_Turma
                       INNER JOIN (SELECT FK_OfertaTurno, FK_Turma, FK_Disciplina, COUNT(PK_ProfessorSumario) Total 
                             FROM {0}P_ProfessorSumario PS 
                               INNER JOIN {0}M_Turma T ON T.PK_Turma = PS.FK_Turma
                               INNER JOIN {0}D_AnoLectivo AN ON AN.Ano = T.AnoLectivo 
                             WHERE T.AnoLectivo = 2016 AND ( PS.DataSumario >= P3DataInicio AND PS.DataSumario <= P3DataFim)
                             GROUP BY FK_Turma, FK_OfertaTurno,  FK_Disciplina) as PS ON PS.FK_Disciplina = MC.FK_Disciplina and ps.FK_Turma = OC.FK_Turma and ps.FK_OfertaTurno = OT.PK_OfertaTurno ", dbProf);
        }

        string IKSTK.GetCSV()
        {
            throw new NotImplementedException();
        }
    }
}
