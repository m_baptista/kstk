﻿using InovarMais.Core;
using InovarMais.Core.Shared;
using System;
using System.Collections.Generic;
using System.Data;


namespace kstkLib
{
    public class MatriculasProfissional : IKSTK
    {
        public string dbProf { get; set; }
        public string dbIdent { get; set; }
        public List<int> lPkAlunos;
        public List<int> lPKEE;

        public MatriculasProfissional()
        {
            dbProf = SQL.GetBdName(BdItem.Profissional) + ".dbo.";
            dbIdent = SQL.GetBdName(BdItem.Ident) + ".dbo.";
            lPkAlunos = new List<int>();
            lPKEE = new List<int>();
        }
        public string GetCSV()
        {
            DataTable dt;
           
                dt = SQL.Q(BdItem.Profissional, GetSQL());
                if (dt.Rows.Count == 0)
                    return "";

                lPkAlunos.Clear();
                lPKEE.Clear();

                foreach (DataRow dr in dt.Rows)
                {
                    var pkAluno = 0;
                    int.TryParse(dr["ERPPK_ALUNO"].ToString().Replace("PROF_", ""), out pkAluno);
                    lPkAlunos.Add(pkAluno);

                    var pkEE = 0;
                    int.TryParse(dr["ERPPK_EE"].ToString().Replace("PROF_", ""), out pkEE);
                    lPKEE.Add(pkEE);
                }
            return Utils.CriaCVS(dt);
        }
        public string GetSQL()
        {
            return string.Format(@"SELECT  
                       DISTINCT 'PROF_' + cast(m.pk_matricula as varchar) 'ERPPK_MATRICULA', 
                       m.FK_Aluno 'ERPPK_ALUNO', 
                       t.AnoLectivo 'ANO_LECTIVO_MATRICULA', 
                       s.Abreviatura 'COD_STATUS_MATRICULA', 
                       s.Descricao 'NOME_STATUS_MATRICULA', 
                       m.DataMatricula 'DATA_STATUS_MATRICULA', 
                       '5' 'COD_RESULTADO_MATRICULA', 
                       'N/A' 'NOME_RESULTADO_MATRICULA', 
                       '' 'DATA_RESULTADO_MATRICULA', 
                       isnull(agrpMISI.Cod_Agrupamento, escMisi.Cod_Escola) 'COD_AGRUPAMENTO', 
                       isnull(agrpMISI.Nome_Agrupamento, escMisi.Nome) 'NOME_AGRUPAMENTO', 
                       escMisi.Cod_Escola 'COD_ESCOLA', 
                       escMisi.Nome 'NOME_ESCOLA', 
                       'PROF_' + cast(t.PK_Turma as varchar)'COD_TURMA', 
                       cast(anEsc.designacao as varchar) + 'º Ano ' + cast(t.Designacao as varchar) 'NOME_TURMA',  
                       anesc.PK_NomeAnoEscolar 'COD_NIVEL_ESCOLAR', 
                       anesc.Designacao 'NOME_NIVEL_ESCOLAR', 
                       C.FK_NivelEnsino 'COD_NIVEL_ESCOLAR_GRUPO', 
                       case anEsc.FK_NivelEnsino WHEN 5 THEN 'SEC' ELSE cast(anEsc.FK_NivelEnsino as varchar) + 'º Ciclo' END 'NOME_NIVEL_ESCOLAR_GRUPO', 
                       c.PK_Curso 'COD_CURSO', 
                       c.Nome  'NOME_CURSO', 
                       CASE WHEN (M.EscalaoAse IS NULL) THEN '--'  ELSE M.EscalaoAse  END AS 'COD_ASE', 
                       CASE WHEN (M.EscalaoAse IS NULL) THEN '--'  ELSE M.EscalaoAse  END AS  'NOME_ASE', 
                       '' 'DATA_INICIO_ASE', 
                       '' 'VALOR_RENDIMENTO', 
                       '' 'VALOR_HABITACAO', 
                       '' 'VALOR_DESPESAS_SAUDE', 
                       i.NElementosAgregado 'NR_PESSOAS_AGREGADO', 
                        'N' 'TEM_ENSINO_ARTICULADO', 
                       CASE ISNULL(m.TemComputador, 0) WHEN 0 THEN 'N' WHEN 1 THEN 'S' ELSE 'N' END  'TEM_COMPUTADOR', 
                       CASE ISNULL(m.TemInternet, 0) WHEN 0 THEN 'N' WHEN 1 THEN 'S' ELSE 'N' END  'TEM_INTERNET', 
                       CASE ISNULL(m.UsaTransporte, 0) WHEN 0 THEN 'N' WHEN 1 THEN 'S' ELSE 'N' END  'TEM_TRANSPORTE_ESCOLAR', 
                       CASE ISNULL(m.AutorizaSairLivre, 0) WHEN 0 THEN 'N' WHEN 1 THEN 'S' ELSE 'N' END  'TEM_SAIDA_LIVRE', 
                       CASE ISNULL(m.AutorizaSairAlmoco, 0) WHEN 0 THEN 'N' WHEN 1 THEN 'S' ELSE 'N' END  'TEM_SAIDA_ALMOCO', 
                       CASE ISNULL(m.AutorizaSairTempo, 0) WHEN 0 THEN 'N' WHEN 1 THEN 'S' ELSE 'N' END  'TEM_SAIDA_ULT_TEMPO', 
                       'PROF_' + cast(m.EncarregadoEducacao as varchar)  'ERPPK_EE' 
                     FROM {0}M_Matricula M 
                       INNER JOIN {0}M_LinhaMatricula LM on FK_Matricula = PK_Matricula 
                       INNER JOIN {0}M_OfertaTurno on PK_OfertaTurno = FK_OfertaTurno 
                       INNER JOIN {0}M_OfertaCurricular on PK_OfertaCurricular = FK_OfertaCurricular 
                       INNER JOIN {0}M_Turma T on PK_Turma = FK_Turma 
                       INNER JOIN {0}C_DistribuicaoCurricular on PK_DistribuicaoCurricular = FK_DistribuicaoCurricular 
                       INNER JOIN {0}C_MatrizCurricular on PK_MatrizCurricular = FK_MatrizCurricular 
                       INNER JOIN {0}C_Curso C on PK_Curso = FK_Curso 
                       INNER JOIN {0}D_SituacaoMatricula s on PK_SituacaoMatricula = FK_SituacaoMatricula 
                       INNER JOIN {1} I_IdentAluno I on PK_IdentAluno = m.FK_Aluno 
                       INNER JOIN {0}D_NomeAnoEscolar anEsc on anEsc.PK_NomeAnoEscolar = t.FK_Ano 
                       INNER JOIN (SELECT TOP 1 * FROM {0}D_ESCOLA) AS ESC ON 1=1   
                       INNER JOIN {0}D_EscolasMISI escMisi ON escMisi.PK_EscolaMISI = ESC.FK_EscolaMISI 
                       LEFT JOIN {0}D_AgrupamentosMISI agrpMISI ON agrpMISI.Cod_Agrupamento = escMisi.Agrup_ID 
                     WHERE t.AnoLectivo >= 2014", dbProf, dbIdent);

        }
    }
}
