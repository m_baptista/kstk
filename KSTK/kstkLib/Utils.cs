﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Collections;
using System.Reflection;
using InovarMais.Core.Shared;
using InovarMais.Core;
using System.Globalization;

namespace kstkLib
{
    public enum ResultadoMatricula
    {
        

        [Description("Transita")]
        Transita = 1,
        [Description("Não Transita")]
        NaoTransita = 2,
        [Description("Aprovado")]
        Aprovado = 3,
        [Description("Não Aprovado")]
        NaoAprovado = 4,
        [Description("N/A")]
        NA = 5,
        [Description("Admitido A Estágio")]
        AE = 6,
        [Description("Pendente")]
        Pendente = 6,
        [Description("Em Proc. Avaliação")]
        EPA = 7,
        [Description("Certificado Escolar")]
        CE = 8,
        [Description("Concluiu")]
        Concluiu = 9,
        [Description("Não Concluiu")]
        NaoConcluiu = 10,
        [Description("Admitido A Exame")]
        AdmitidoExame = 11,
        [Description("Não concluiu")]
        Naoconcluiu = 10,
        [Description("Situação Pendente")]
        SitPendente = 6
    }

    public enum TipoFaltas
    {
        [Description("Injustificada")]
        FI = 1,
        [Description("Material")]
        MT = 2,
        [Description("Disciplinar")]
        FD = 3,
        [Description("Justificada")]
        FJ = 4,
        [Description("Pontualidade")]
        FP = 5,
        [Description("Trabalho para casa")]
        TPC = 6
    }

    public class Utils
    {
        
        public static T GetCodigoEnum<T>(string Descricao)
        {
            var type = typeof(T);
            if (!type.IsEnum) throw new InvalidOperationException();
            foreach (var field in type.GetFields())
            {
                var attribute = Attribute.GetCustomAttribute(field,
                    typeof(DescriptionAttribute)) as DescriptionAttribute;
                if (attribute != null)
                {
                    if (attribute.Description == Descricao)
                        return (T)field.GetValue(null);
                }
                else
                {
                    if (field.Name == Descricao)
                        return (T)field.GetValue(null);
                }
            }
            throw new ArgumentException("Not found.", "description");
            // or return default(T);
        }

        public static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        public static string CriaCVS(DataTable dt, int ind =0)
        {
            StringBuilder strCVS = new StringBuilder();

            if (dt.Rows.Count == 0)
                return strCVS.ToString();

            foreach (DataRow item in dt.Rows)
            {
                var alLinhas = new ArrayList();
                var linha = item.ItemArray;
                for (int i = 0; i < linha.Length-(1+ind); i++)
                {
                    if( linha[i].GetType().Name == "DateTime")
                        alLinhas.Add(((char)34) + ((DateTime)linha[i]).ToString("yyyy-MM-dd") + ((char)34));
                    else
                        alLinhas.Add(((char)34) + linha[i].ToString() + ((char)34));
                }
                if(alLinhas.Count > 0)
                    strCVS.AppendLine(string.Join(",", alLinhas.ToArray()));
            }
            dt.Dispose();
            return strCVS.ToString();
        }

        public static List<string> CriaCVS2(DataTable dt, int ind = 0)
        {
            var strCVS = new List<string>();

            if (dt.Rows.Count == 0)
                return null;

            foreach (DataRow item in dt.Rows)
            {
                var alLinhas = new ArrayList();
                var linha = item.ItemArray;
                for (int i = 0; i < linha.Length - (1 + ind); i++)
                {
                    if (linha[i].GetType().Name == "DateTime")
                        alLinhas.Add(((char)34) + ((DateTime)linha[i]).ToString("yyyy-MM-dd") + ((char)34));
                    else
                        alLinhas.Add(((char)34) + linha[i].ToString() + ((char)34));
                }
                if (alLinhas.Count > 0)
                    strCVS.Add(string.Join(",", alLinhas.ToArray()));
            }
            dt.Dispose();
            return strCVS;
        }

        public static string BuildIn(IEnumerable<int> lst)
        {
            if (lst == null || lst.Count() == 0)
                return "(-1)";

            return "(" + string.Join(",", lst.ToArray()) + ")";
        }

        public static string GetCSV(BdItem bd, string _sql)
        {
            DataTable dt;
            try
            {
                dt = SQL.Q(bd, _sql);
                if (dt.Rows.Count == 0)
                    return "";
            }
            catch (Exception ex)
            {
                return "";
            }
            return Utils.CriaCVS(dt);
        }

        public static List<string> GetCSV2(BdItem bd, string _sql)
        {
            DataTable dt;
            try
            {
                dt = SQL.Q(bd, _sql);
                if (dt.Rows.Count == 0)
                    return null;
            }
            catch (Exception ex)
            {
                return null;
            }
            return Utils.CriaCVS2(dt);
        }

        public static string DaDataParaFicheiro()
        {
            var data = DateTime.Now;

            return data.Year + xx(data.Month) + xx(data.Day) + xx(data.Hour) + xx(data.Minute) + xx(data.Second);
        }

        public static Situacao CalculaSituacao(string situacaoMatricula, int nivel)
        {
            TextInfo ti = CultureInfo.CurrentCulture.TextInfo;

            var _situacaoMatricula = ti.ToTitleCase(situacaoMatricula);
            if (nivel == 13 || nivel == 10 || nivel == 8)
                return CalculaSituacaoFinalCiclo(_situacaoMatricula);
            else if (nivel == 16)
                return CalculaSituacaoFinal12Ano(_situacaoMatricula);
            else
                return CalculaSituacaoFinalRestantes(_situacaoMatricula);

        }

        private static string xx(int num)
        {
            if (num < 10)
                return "0" + num.ToString();

            return num.ToString();
        }

        public static Situacao CalculaSituacaoFinalCiclo(string situacaoMatricula)
        {
            var situacao = new Situacao();
            var codigoAverbamento = (int)Utils.GetCodigoEnum<ResultadoMatricula>(situacaoMatricula);
            if (codigoAverbamento == 1 | codigoAverbamento == 3)
            {
                situacao.Codigo = (int)ResultadoMatricula.Aprovado;
                situacao.Descricao = GetEnumDescription(ResultadoMatricula.Aprovado);
            }
            else if (codigoAverbamento == 2 | codigoAverbamento == 4)
            { 
                situacao.Codigo = (int)ResultadoMatricula.NaoAprovado;
                situacao.Descricao = GetEnumDescription(ResultadoMatricula.NaoAprovado);
            }
            else
            {
                situacao.Codigo = codigoAverbamento;
                situacao.Descricao = situacaoMatricula;
            }
            return situacao;
        }

        public static Situacao CalculaSituacaoFinal12Ano(string situacaoMatricula)
        {
            var situacao = new Situacao();
            var codigoAverbamento = (int)Utils.GetCodigoEnum<ResultadoMatricula>(situacaoMatricula);
            if (codigoAverbamento == 1 | codigoAverbamento == 3)
            {
                situacao.Codigo = (int)ResultadoMatricula.Concluiu;
                situacao.Descricao = GetEnumDescription(ResultadoMatricula.Concluiu);
            }
            else if(codigoAverbamento == 2 | codigoAverbamento == 4)
            {
                situacao.Codigo = (int)ResultadoMatricula.NaoConcluiu;
                situacao.Descricao = GetEnumDescription(ResultadoMatricula.NaoConcluiu);
            }
            else
            {
                situacao.Codigo = codigoAverbamento;
                situacao.Descricao = situacaoMatricula;
            }
            return situacao;
        }

        public static Situacao CalculaSituacaoFinalRestantes(string situacaoMatricula)
        {
            var situacao = new Situacao();
            var codigoAverbamento = (int)Utils.GetCodigoEnum<ResultadoMatricula>(situacaoMatricula);
            if (codigoAverbamento == 1 | codigoAverbamento == 3)
            {
                situacao.Codigo = (int)ResultadoMatricula.Transita;
                situacao.Descricao = GetEnumDescription(ResultadoMatricula.Transita);
            }
            else if (codigoAverbamento == 2 | codigoAverbamento == 4)
            {
                situacao.Codigo = (int)ResultadoMatricula.NaoTransita;
                situacao.Descricao = GetEnumDescription(ResultadoMatricula.NaoTransita);
            }
            else
            {
                situacao.Codigo = codigoAverbamento;
                situacao.Descricao = situacaoMatricula;
            }
            return situacao;
        }

    }
    public class Situacao
    {
        public int Codigo { get; set; }
        public string Descricao { get; set; }
    }
}
