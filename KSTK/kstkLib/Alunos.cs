﻿using InovarMais.Core;
using InovarMais.Core.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kstkLib
{
    public class Alunos
    {
        private string dbIdent { get; set; }
        private string dbMisi { get;  set; }
        private string strIds { get; set; }
        public Alunos(List<int> lPkAlunos)
        {
            dbIdent = SQL.GetBdName(BdItem.Ident) + ".dbo.";
            dbMisi = SQL.GetBdName(BdItem.MISI) + ".dbo.";

            strIds = Utils.BuildIn(lPkAlunos);

        }
        public string GetCVS()
        {
            DataTable dt;
            
                dt = SQL.Q(BdItem.Ident, GetSQL());
                if (dt.Rows.Count == 0)
                    return "";

            

            return Utils.CriaCVS(dt);
        }
        private string GetSQL()
        {
            return string.Format( @" Select 
                          PK_IdentAluno 'ERPPK_ALUNO', 
                          NumeroProcesso 'NR_ALUNO', 
                          NOME 'NOME_ALUNO', 
                          NFiscal 'NIF_ALUNO', 
                          SEXO 'SEXO_ALUNO', 
                          CONVERT(DATE, NASCIMENTO ,102)'DATA_NASC_ALUNO', 
                          ID_Nacionalidade 'NATURALIDADE_COD_PAIS_ALUNO', 
                          N.Nacionalidade 'NATURALIDADE_NOME_PAIS_ALUNO', 
                          RESIDENCIA 'MORADA_ALUNO', 
                          isnull(CP.Codigo,'0000') +'-' + isnull(cp.CP3, '000') 'MORADA_CPOSTAL_ALUNO', 
                          CP.Localidade 'MORADA_CPOSTAL_DESC_ALUNO', 
                          '' 'MORADA_COD_PAIS_ALUNO', 
                          '' 'MORADA_NOME_PAIS_ALUNO', 
                          EMAIL 'MAIL_ALUNO', 
                          TELEFONE1 'TELEFONE_ALUNO', 
                          SubsidioEntidade 'COD_SEG_SOCIAL', 
                          AF.Descricao 'NOME_SEG_SOCIAL', 
                          isnull(SubsidioNumero, '') 'NR_SEG_SOCIAL' 
                     FROM {1}i_identAluno  
                          LEFT JOIN {2}Nacionalidade_MISI N ON PK_NacionalidadeMISI = ID_Nacionalidade 
                          LEFT JOIN {2}T_CODIGOS_POSTAIS CP ON cp.ID = ID_Codigo_Postal 
                          LEFT JOIN {2}TipoAbonoFamilia AF ON AF.PK_TipoAbonoFamilia = SubsidioEntidade 
                     WHERE PK_IdentAluno IN  {0}", strIds,dbIdent, dbMisi);
        }
    }
}
