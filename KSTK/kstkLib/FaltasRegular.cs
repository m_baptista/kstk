﻿using InovarMais.Core;
using InovarMais.Core.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kstkLib
{
    public class FaltasRegular : IKSTK
    {
        string dbAluno; 
        public FaltasRegular()
        {
            dbAluno = SQL.GetBdName(BdItem.Regular) + ".dbo.";
        }
        public List<string> GetCSV()
        {
            DataTable dt;
           dt = SQL.Q(BdItem.Regular, GetSQL());
                if (dt.Rows.Count == 0)
                    return null;

                dt.Columns["NOME_TIPO_FALTA"].ReadOnly = false;
                dt.Columns["NOME_TIPO_FALTA"].MaxLength = 100;

                foreach (DataRow dr in dt.Rows)
                {
                    var pkTipoFalta = 0;
                    int.TryParse(dr["COD_TIPO_FALTA"].ToString(), out pkTipoFalta);
                    string nomeFalta = Utils.GetEnumDescription((TipoFaltas) pkTipoFalta);
                    dr["NOME_TIPO_FALTA"] = nomeFalta;

                }
           
            return Utils.CriaCVS2(dt);

        }
        public string GetSQL()
        {
            return string.Format(@" select 'REG_' + cast(PK_MatriculaFalta as varchar)  'ERPPK_FALTA', 
                       FK_Aluno 'ERPPK_ALUNO', 
                       t.AnoLectivo 'ANO_LECTIVO_MATRICULA', 
                      'REG_' + cast(cm.FK_Disciplina as varchar) 'COD_DISCIPLINA',
                       d.NomeExtenso 'NOME_DISCIPLINA', 
                       convert(date, mf.DataFalta, 102) 'DATA_FALTA', 
                       mf.HoraInicio 'HORA_FALTA', 
                       mf.FK_TipoFalta 'COD_TIPO_FALTA', 
                       ''  'NOME_TIPO_FALTA', 
                       ''  'ERPPK_PROF_FALTA' 
                     from {0}M_MatriculaFalta MF 
                       inner join {0}M_LinhaMatricula on PK_LinhaMatricula = FK_LinhaMatricula 
                       inner join {0}M_Matricula M on PK_Matricula = FK_Matricula 
                       inner join {0}M_Turma T on T.PK_Turma = M.FK_Turma 
                       inner join {0}M_OfertaTurno on FK_OfertaTurno= PK_OfertaTurno 
                       inner join {0}M_OfertaCurricular oc on oc.PK_OfertaCurricular = FK_OfertaCurricular 
                       inner join {0}C_MatrizCurricular cm on cm.PK_MatrizCurricular = FK_MatrizCurricular 
                       inner join {0}C_Disciplina D on PK_Disciplina = FK_Disciplina 
                     where AnoLectivo >= 2014 ", dbAluno);

        }

        string IKSTK.GetCSV()
        {
            throw new NotImplementedException();
        }
    }
}
