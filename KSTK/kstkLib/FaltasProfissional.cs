﻿using InovarMais.Core;
using InovarMais.Core.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kstkLib
{
    public class FaltasProfissional 
    {
        string dbAluno;
        public FaltasProfissional()
        {
            dbAluno = SQL.GetBdName(BdItem.Profissional) + ".dbo.";
        }
        public List<string> GetCSV(int ano)
        {
            DataTable dt;
       
                dt = SQL.Q(BdItem.Profissional, GetSQL(ano));
                if (dt.Rows.Count == 0)
                    return null;

                dt.Columns["NOME_TIPO_FALTA"].ReadOnly = false;
                dt.Columns["NOME_TIPO_FALTA"].MaxLength = 100;
                dt.Columns["HORA_FALTA"].ReadOnly = false;
                                
                foreach (DataRow dr in dt.Rows)
                {
                    var pkTipoFalta = 0;
                    int.TryParse(dr["COD_TIPO_FALTA"].ToString(), out pkTipoFalta);
                    string nomeFalta = Utils.GetEnumDescription((TipoFaltas)pkTipoFalta);
                    dr["NOME_TIPO_FALTA"] = nomeFalta;

                    int pkTempo = 0;
                    int.TryParse(dr["HORA_FALTA"].ToString(), out pkTempo);
                   
                    dr["HORA_FALTA"] = CalculaTempo(pkTempo);
                    

                }
       
            return Utils.CriaCVS2(dt);

        }
        public string GetSQL(int ano)
        {
            return string.Format(@" select distinct    
                       'PROF_' + cast(PK_LinhaMatriculaFalta as varchar) 'ERPPK_FALTA', 
                       FK_Aluno 'ERPPK_ALUNO',
                       T.AnoLectivo 'ANO_LECTIVO_MATRICULA', 
                       'PROF_' + cast(PK_Disciplina as varchar) 'COD_DISCIPLINA', 
                       D.NomeExtenso 'NOME_DISCIPLINA', 
                       convert(date, DataFalta, 102)'DATA_FALTA', 
                       cast(Tempo as varchar) 'HORA_FALTA', 
                       Fk_TipoFalta 'COD_TIPO_FALTA', 
                       '' 'NOME_TIPO_FALTA', 
                       PT.FK_Professor 'ERPPK_PROF_FALTA' 
                     from {0}M_LinhaMatriculaFalta 
                       inner join {0}M_LinhaMatricula LM on PK_LinhaMatricula = FK_LinhaMatricula 
                       inner join {0}M_Matricula on PK_Matricula = FK_Matricula 
                       inner join {0}M_TempoTipoFalta TP on FK_LinhaMatriculaFalta = PK_LinhaMatriculaFalta 
                       inner join {0}M_OfertaTurno on PK_OfertaTurno = LM.FK_OfertaTurno 
                       inner join {0}M_OfertaCurricular OC on PK_OfertaCurricular = FK_OfertaCurricular 
                       inner join {0}C_DistribuicaoCurricular on PK_DistribuicaoCurricular = FK_DistribuicaoCurricular 
                       inner join {0}C_MatrizCurricular on PK_MatrizCurricular = FK_MatrizCurricular 
                       inner join {0}M_Turma T on OC.FK_Turma = PK_Turma 
                       inner join {0}C_Disciplina D on PK_Disciplina = FK_Disciplina  
                       inner join {0}M_ProfessorTurno PT on PK_OfertaTurno = PT.FK_OfertaTurno
                     where AnoLectivo = {1} ", dbAluno, ano) ;

        }
        private string CalculaTempo(int numTempo)
        {
            var hora = new DateTime(1, 1, 1, 8, 15, 0);
            for (int i = 1; i <= numTempo; i++)
            {
                hora.AddMinutes(45);
            }
            return hora.ToShortTimeString();
        }
    }
}
