﻿using InovarMais.Core;
using InovarMais.Core.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kstkLib
{
    public class EncarregadosEducacao : IKSTK
    {
        private string dbIdent { get; set; }
        private string dbMisi { get; set; }
        private string strIds { get; set; }

        public EncarregadosEducacao(List<int> lPKEE)
        {
            dbIdent = SQL.GetBdName(BdItem.Ident) + ".dbo.";
            dbMisi = SQL.GetBdName(BdItem.MISI) + ".dbo.";

            strIds = Utils.BuildIn(lPKEE);
        }

        public string GetCSV()
        {
            DataTable dt;
           
                dt = SQL.Q(BdItem.Ident, GetSQL());
                if (dt.Rows.Count == 0)
                    return "";

            
            return Utils.CriaCVS(dt);
        }
        public string GetSQL()
        {
            return string.Format(@" SELECT  
                          PK_EncarregadoEducacao 'ERPPK_EE',
                          Nome 'NOME_EE', 
                          NIF 'NIF_EE', 
                          p.Descricao 'PARENTESCO_EE', 
                          CASE Tipo WHEN 1 THEN 'M' WHEN 2 THEN 'F' WHEN 3 THEN 'M' WHEN 4 THEN 'F' WHEN 5 THEN  'M' WHEN 6 THEN 'F' WHEN 7 THEN 'M' WHEN 8 THEN 'F' WHEN 9 THEN '' END 'SEXO_EE', 
                          '' 'DATA_NASC_EE', 
                          FK_Naturalidade 'NATURALIDADE_COD_PAIS_EE', 
                          N.Nacionalidade 'NATURALIDADE_NOME_PAIS_EE', 
                          Residencia 'MORADA_EE', 
                          CP.Codigo + '-' + ISNULL(CP.CP3,'000')  'MORADA_CPOSTAL_EE', 
                          CP.Localidade 'MORADA_CPOSTAL_DESC_EE', 
                          '' 'MORADA_COD_PAIS_EE', 
                          '' 'MORADA_NOME_PAIS_EE',
                          EMAIL 'MAIL_EE', 
                          Telefone 'TELEFONE_EE', 
                          isnull(FK_FormacaoProfissional, 99999) 'COD_FORM_ACADEMICA_EE', 
                          isnull(FA.FormacaoAcademica, 'SEM DADOS') 'NOME_FORM_ACADEMICA_EE', 
                          isnull(FK_Profissao, 999999) 'COD_EMPREGO_EE', 
                          isnull(PF.Profissao,'SEM DADOS') 'NOME_EMPREGO_EE', 
                          isnull(PC.CodClasseProf, 99999) 'COD_AREA_PROFISSIONAL_EE', 
                          isnull(PC.Classe_Profissao, 'SEM DADOS') 'NOME_AREA_PROFISSIONAL_EE', 
                          isnull(SE.CodSitEmp, 99999) 'COD_STATUS_EMPREGO_EE', 
                          isnull(SE.SituacaoEmp, 'SEM DADOS') 'NOME_STATUS_EMPREGO_EE' 
                     FROM {1}D_EncarregadoEducacao  
                          LEFT JOIN {1}U_Parentesco P on P.PK_Parentesco = Tipo 
                          LEFT JOIN {2}Nacionalidade_MISI N ON PK_NacionalidadeMISI = FK_Nacionalidade 
                          LEFT JOIN {2}T_CODIGOS_POSTAIS CP ON cp.ID = FK_CodigoPostal 
                          LEFT JOIN {2}FormacaoAcademica_MISI FA ON FA.PK_FormacaoAcademicaMISI  = FK_FormacaoProfissional 
                          LEFT JOIN {2}Profissao_MISI PF ON PF.PK_ProfissaoMISI = FK_Profissao 
                          LEFT JOIN {2}ClasseProfissao_MISI PC ON PC.PK_ClasseProfissaoMISI = PF.Classe_id 
                          LEFT JOIN {2}SituacaoEmprego_MISI SE ON SE.PK_SituacaoEmpregoMISI = FK_SituacaoEmprego 
                     WHERE PK_EncarregadoEducacao IN {0}", strIds,dbIdent,dbMisi);
        }

    }
}
